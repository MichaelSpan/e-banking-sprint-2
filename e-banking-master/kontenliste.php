<?php
require_once ("models/Account.php");
include ("header.php");
$acc = new Account("","");

require_once ("models/CookieHelper.php");
require_once ("models/User.php");

if (!CookieHelper::isValid() | !User::isLoggedIn()) {
    header("Location:index.php");
}?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>eBankingApp</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<h1>Kontenliste!</h1>
<table class="table table-striped  table-hover col-sm-12  col-md-6 text-center">
    <thead>
    <tr>
        <th>IBAN</th>
        <th>Kontonummer</th>
        <th>Kontoinhaber</th>
        <th>Kontostand</th>
        <th>Anwendungen</th>
    </tr>
    <tr>
     <a href="addAccount.php"> <p class="btn btn-primary btn-block"> Account anlegen</p></a>

    </tr>
    </thead>
    <tbody>





</body>