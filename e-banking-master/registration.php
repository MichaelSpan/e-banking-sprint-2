<?php
require_once ("models/CookieHelper.php");
include ("header.php");

if (!CookieHelper::isValid()) {
    header("Location:index.php");
}?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <title>eBankingApp</title>

</head>
<body>
<div class="container-fluid">

    <h1 class="text-primary mt-5 text-center border border-dark"><b>eBanking-App</b></h1>

    <form id="form_registration" action="registration.php" method="post">
        <div class="row mt-5">
            <div class="col-sm-7 ml-5 mt-2 form-group">
                <div class="row">
                    <h2 class="ml-3 mt-5"><b>Registrieren</b></h2>
                </div>
                <div class="col-sm-6 mt-3 form-group">
                    <div class="row">
                        <label for="firstname">Vorname*</label>
                        <input type="text"
                               id="firstname"
                               name="firstname"
                               maxlength="25"
                               class="form-control <?= isset($errors['firstname']) ? 'is-invalid' : '' ?>"

                               required="required">
                    </div>
                </div>
                <div class="col-sm-6 mt-4 form-group">
                    <div class="row">
                        <label for="lastname">Nachname*</label>
                        <input type="text"
                               id="lastname"
                               name="lastname"
                               maxlength="25"
                               class="form-control <?= isset($errors['lastname']) ? 'is-invalid' : '' ?>"
                               required="required">
                    </div>
                </div>
                <div class="col-sm-6 mt-4 form-group">
                    <div class="row">
                        <label for="username">Benutzername*</label>
                        <input type="text"
                               id="username"
                               name="username"
                               maxlength="25"
                               class="form-control <?= isset($errors['username']) ? 'is-invalid' : '' ?>"
                               required="required">
                    </div>
                </div>
                <div class="col-sm-6 mt-4 form-group">
                    <div class="row">
                        <label for="email">E-Mail*</label>
                        <input type="email"
                               id="email"
                               name="email"
                               class="form-control <?= isset($errors['email']) ? 'is-invalid' : '' ?>
                               required="required">
                    </div>
                </div>
                <div class="col-sm-6 form-group">
                    <div class="row">
                        <label for="password">Passwort*</label>
                        <input type="password"
                               id="password"
                               name="password"
                               maxlength="25"
                               class="form-control <?= isset($errors["password"]) ? "is-invalid" : "" ?>"

                               required="required">
                    </div>
                </div>
                <div class="col-sm-6 mt-4 form-group">
                    <div class="row">
                        <div class="col-sm-6 form-group">
                            <input type="submit"
                                   name="submit"
                                   class="btn btn-primary btn-block"
                                   value="Benutzer Anlegen">
                        </div>
                        <div class="col-sm-6 form-group">
                            <a href="index.php" class="btn btn-secondary btn-block">Zurück</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</form>
</div>
</body>
