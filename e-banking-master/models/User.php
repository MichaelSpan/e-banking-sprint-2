<?php
class User
{
    /**
     * @var $email
     */
    private $email = '';

    /**
     * @var $password
     */
    private $password = '';

    private $isStaff;

    /**
     * @var $errors array
     */
    private $errors = [];

    /**
     * @param string $email
     */
    public function __construct(string $email, string $password, bool $isStaff)
    {
        $this->email = $email;
        $this->password = $password;
        $this->isStaff = $isStaff;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getIsStaff()
    {
        return $this->isStaff;
    }

    /**
     * @param mixed $isStaff
     */
    public function setIsStaff($isStaff)
    {
        $this->isStaff = $isStaff;
    }

    /**
     * @param $field
     * @return bool
     */
    public function hasError($field)
    {
        return isset($this->errors[$field]);
    }

    public function login()
    {
        if ($this->validate()) {
            $user = serialize($this);
            $_SESSION['user'] = $user;
            return true;
        }
        return false;
    }

    public static function logout()
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
            session_destroy();
        }
    }

    public static function isLoggedIn()
    {
        if (isset($_SESSION['user'])) {
            return true;

        } else {
            return false;
        }
    }

    public function validate()
    {
        return $this->validateEmail($this->getEmail()) & $this->validatePassword($this->getPassword());
    }

    private function validateEmail(String $email)
    {
        if ($this->email != '' && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->errors['email'] = 'E-Mail Adresse ungültig';
            return false;
        } else if (strlen($this->email) < 5 || strlen($this->email) > 30) {
            $this->errors['email'] = 'E-Mail Adresse zu kurz bzw. zu lang';
            return false;
        } else if (!($this->email = $email)) {
            $this->errors['email'] = "Falsche E-Mail Addresse!";
            return false;

        } else {
            return true;
        }
    }

    private function validatePassword($password)
    {
        if (strlen($this->password) == 0) {
            $this->errors['password'] = 'Password darf nicht leer sein';
            return false;
        } else if (strlen($this->password) < 5 || strlen($this->password) > 20) {
            $this->errors['password'] = 'Password zu kurz bzw. zu lang';
            return false;
        } else if (!($this->password == $password)) {
            $this->errors['password'] = "Falsches Passwort!";
            return false;
        } else {
            return true;
        }
    }

    public function validateStaff()
    {
        if ($this->getIsStaff() == true) {
            return true;
        } else {
            return false;
        }
    }
}