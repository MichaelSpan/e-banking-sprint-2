<?php
class CookieHelper
{
    public function __construct()
    {

    }

    public static function isValid()
    {
        return isset($_SESSION["validated"]);
    }

    public function confirmValid(bool $valid)
    {
        $_SESSION["validated"] = $valid;
    }
}