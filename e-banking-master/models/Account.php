<?php

class Account
{

    private $balance = '';
    private $iban = '';
    private $bic = '';
    private $owner= '';
    private $isactiv=true;
    private $kontoliste = array();

    /**
     * @param string $balance
     * @param string $owner
     */
    public function __construct(string $balance, string $owner)
    {
        (float) $iban = rand(10,20);
        $this->balance = $balance;
        $this->owner = $owner;
    }

    public function  getAll()
    {
        return array();
    }
    /**
     * @return string
     */
    public function getBalance(): string
    {
        return $this->balance;
    }

    /**
     * @param string $balance
     */
    public function setBalance(string $balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     */
    public function setIban(string $iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return string
     */
    public function getBic(): string
    {
        return $this->bic;
    }

    /**
     * @param string $bic
     */
    public function setBic(string $bic)
    {
        $this->bic = $bic;
    }

    /**
     * @return string
     */
    public function getOwner(): string
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     */
    public function setOwner(string $owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return bool
     */
    public function isIsactiv(): bool
    {
        return $this->isactiv;
    }

    /**
     * @param bool $isactiv
     */
    public function setIsactiv(bool $isactiv)
    {
        $this->isactiv = $isactiv;
    }




}