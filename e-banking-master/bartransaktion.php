<?php
require_once ("models/CookieHelper.php");
require_once ("models/User.php");
include ("header.php");

if (!CookieHelper::isValid() | !User::isLoggedIn()) {
    header("Location:index.php");
}?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>eBankingApp</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<?php
include ("header.php");
?>
<h1>BarTransaktion!</h1>
<table>
    <tbody>
    <tr>
        <td>Betrag</td>
        <td><input type="number"></td>
    </tr>
    <tr>
        <td><p class="btn btn-primary btn-block"> Geld einzahlen...</p> </td>
        <td><p class="btn btn-primary btn-block"> Geld auszhlen...</p></td>
        <td><a href="kontoansicht.php"><p class="btn btn-primary btn-block"> zurück...</p></a></td>
    </tr>
    </tbody>
</table>


</body>