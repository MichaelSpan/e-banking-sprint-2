<?php
require_once ("models/Account.php");
include ("header.php");
$acc = new Account("","");

require_once ("models/CookieHelper.php");
require_once ("models/User.php");

if (!CookieHelper::isValid() | !User::isLoggedIn()) {
    header("Location:index.php");
}?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>eBankingApp</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<h1>Kontoansicht</h1>

<table class="table table-striped  table-hover col-sm-12  col-md-6 text-center">
    <thead>
    <tr>
        <th>IBAN</th>
        <th>Kontonummer</th>
        <th>Kontoinhaber</th>
        <th>Kontostand</th>
        <th>Anwendungen</th>
        <th>Transaktionen</th>

        <a href="bartransaktion.php"> <p class="btn btn-primary btn-block"> Geld managen...</p></a>
        <a href="transaktion.php"> <p class="btn btn-primary btn-block"> Transnaktionen managen...</p></a>

    </tr>
    </thead>
    <tbody>


</body>