<?php
require_once ("models/CookieHelper.php");
require_once ("models/User.php");
include ("header.php");

if (!CookieHelper::isValid() | !User::isLoggedIn()) {
    header("Location:index.php");
}?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>eBankingApp</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<?php
include ("header.php");
?>
<h1>Erweiterte Suche!</h1>
<table>
    <tbody>
    <tr>
        <td>Von....</td>
        <td><input type="date" id="start">
        </td>
    </tr>
    <tr>
        <td>Bis....</td>
        <td><input type="date" id="ending"> </td>
    </tr>
    <tr>
        <td>Menge..</td>
        <td><input type="number"> </td>
    </tr>
    <tr>
        <td>Text...</td>
        <td><input type="text"></td>
    </tr>
    </tbody>
</table>
 <p class="btn btn-primary btn-block"> suchen....</p>
<a href="kontoansicht.php"> <p class="btn btn-primary btn-block"> zurück....</p></a>
 <p class="btn btn-primary btn-block"> drucken....</p>
<table class="table table-striped  table-hover col-sm-12  col-md-6 text-center">
    <thead>
    <tr>
        <th>IBAN</th>
        <th>Transactionsnummer</th>
        <th>Betrag</th>
        <th>Text</th>

    </tr>
    <tr>


    </tr>
    </thead>
    <tbody>


</body>