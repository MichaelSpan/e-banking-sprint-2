<?php

require_once "models/user.php";
require_once "models/CookieHelper.php";


$check = new User(htmlspecialchars($_COOKIE['e-mail']), htmlspecialchars($_COOKIE['password'], htmlspecialchars($_COOKIE['staff'])));
if ($check->validate()) {
    $user = $check;
    setcookie('isStaff', $user->isStaff(),time() + (300),"/");
    $user->login();
}
?>