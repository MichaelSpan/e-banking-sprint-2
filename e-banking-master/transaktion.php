<?php
require_once ("models/CookieHelper.php");
require_once ("models/User.php");
include ("header.php");

if (!CookieHelper::isValid() | !User::isLoggedIn()) {
    header("Location:index.php");
}?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>eBankingApp</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<?php
include ("header.php");
?>
<h1>Transaktion</h1>
<table class="table table-striped  table-hover col-sm-12  col-md-6 text-center">
    <thead>
    <tr>
        <th>ID</th>
        <th>Konto</th>
        <th>Betrag</th>
        <th>Text</th>

    </tr>
    <tr>
        <a href="erweiterteSuche.php"> <p class="btn btn-primary btn-block"> Erweiterte Suche</p></a>
        <a href="kontoansicht.php"> <p class="btn btn-primary btn-block"> zurück....</p></a>
        <p class="btn btn-primary btn-block"> drucken....</p>

    </tr>
    </thead>
    <tbody>





</body>
