<?php
require_once ("models/Account.php");

require_once("models/CookieHelper.php");
require_once("models/User.php");
include("header.php");

if (!CookieHelper::isValid() | !User::isLoggedIn()) {
    header("Location:index.php");
} ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>eBankingApp</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<h1>Account Hinzufügen....</h1>
<table>
    <tbody>
    <tr>
        <td>Owner</td>
        <td><input type="text"></td>
    </tr>
    <tr>
        <td>Balance</td>
        <td><input type="text"></td>
    </tr>
    <tr>
        <td>BIC</td>
        <td><input type="text"></td>
    </tr>

    <tr>
        <td>IBAN</td>
        <td><input type="text"></td>
    </tr>
    <tr>
    <div class="col-sm-6 mt-4 form-group">
        <div class="row">
            <div class="col-sm-6 form-group">
                <input type="submit"
                       name="submit"
                       class="btn btn-primary btn-block"
                       value="Account Anlegen">
            </div>
            <div class="col-sm-6 form-group">
                <a href="kontenliste.php" class="btn btn-secondary btn-block">Zurück</a>
            </div>
        </div>
    </div>
    </tr>
    </tbody>
</table>


</body>