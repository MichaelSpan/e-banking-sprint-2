<?php
session_start();
require_once "models/CookieHelper.php";
require_once "models/User.php";
$cookie = new CookieHelper();
$user = new User("", "", "");
$check = new User("", "", "");
$message = '';
if (isset($_POST["valid"])) {
    $cookie->confirmValid(true);
}

$test_staff = new User("micspan@tsn.at", "123456", true);
$test_user = new User("stobererlacher@tsn.at", "654321", false);

if (isset($_POST["login"])) {
    if (htmlspecialchars($_POST["email"])==$test_staff->getEmail()&&htmlspecialchars($_POST["password"])==$test_staff->getPassword())
    {
        $check = $test_staff;
    } elseif (htmlspecialchars($_POST["email"])==$test_user->getEmail()&&htmlspecialchars($_POST["password"])==$test_user->getPassword())
    {
        $check = $test_user;
    }
    if ($check->validate()) {
        $user = $check;
        setcookie('email', $user->getEmail(), time() + (300), "/");
        setcookie('password', $user->getPassword(), time() + (300), "/");
        setcookie('isStaff', $user->getIsStaff(), time() + (300), "/");
        $user->login();
    } else
    {
        $message="<p class='text-danger'>Ungültige Daten!</p>";
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Super-eBanking</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <?php
    if (!CookieHelper::isValid()) {
        ?>
        <div class="col-sm-12 mt-5" style="text-align: center">
            <h1 style="text-align: center">Super-eBanking</h1>
            <div class="mt-5 align-content-center">
                <h3>Willkommen</h3>
                <h5>Diese Website verwendet Cookies.</h5>
            </div>
        </div>
        <form id="form_accept" action="index.php" method="post">
            <div class="row mt-3">
                <div class="col-sm-4"></div>
                <div class="col-sm-4 justify-content-center d-flex">
                    <input type="submit" name="valid" class="btn btn-primary btn-block" value="Akzeptieren"/>
                </div>
            </div>
        </form>
        <?php
    } elseif (CookieHelper::isValid() && !User::isLoggedIn()) {
    ?>
    <h1 class="text-primary mt-5 text-center border border-dark"><b>Super-eBanking</b></h1>

    <form id="form_ebanking" action="index.php" method="post">
        <div class="row mt-5">
            <div class="col-sm-5 mt-5 form-group">
                <div class="row">
                    <h2 class="ml-3"><b>Login</b></h2>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-6 form-group">
                        <label for="name">E-Mail*</label>
                        <input type="text"
                               id="email"
                               name="email"
                               maxlength="25"
                               class="form-control <?= isset($errors['username']) ? 'is-invalid' : '' ?>"
                               required="required">
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="password">Passwort*</label>
                        <input type="text"
                               id="password"
                               name="password"
                               maxlength="25"
                               class="form-control <?= isset($errors["password"]) ? "is-invalid" : "" ?>"
                               required="required">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3 form-group">
                        <input type="submit"
                               name="login"
                               class="btn btn-primary btn-block"
                               value="Login">
                    </div>
                    <div class="col-sm-3 form-group">
                        <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-7 mt-5 form-group">
                <div class="row">
                    <h2 class="ml-5"><b>Noch kein Benutzerkonto?</b></h2>
                </div>
                <div class="row">
                    <div class="col-sm-5 form-group">
                        <a href="registration.php" class="mt-4 ml-5 btn btn-outline-primary btn-block">Neuen Benutzer anlegen</a>
                    </div>
                </div>
            </div>
        </div>
        <?php
        } else {
            if ($check->getIsStaff()==true)
            {
                header("Location: kontenliste.php");
            } else {
                header("Location: kontoansicht.php");
            }
        }
        ?>
</div>
</div>
</form>
</div>
</body>